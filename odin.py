# Change these values at will
# Locked ODIN in millions
locked_odin = 77
# Current block
current_block = 14603
# Blocktime in seconds
blocktime = 60
# Amount of ODIN you hold
odin = 120000
# How many ODIN you are staking
staking = 20000
# How many masternodes you are running
mn = 4
# Part of masternodes online
mn_online = 0.8
# Part of ODIN not in masternodes being staked
staking_online = 0.6

if (mn * 25000 > odin):
    print("Can't run " + str(mn) + " masternode(s) with " + str(odin) + " ODIN.")
    exit()
if (mn * 25000 + staking > odin):
    print("Can't run " + str(mn) + " masternode(s) and stake " + str(staking) + " ODIN with " + str(odin) + " ODIN.")
    exit()
if (staking > odin):
    print("Can't stake " + str(staking) + " ODIN with " + str(odin) + " ODIN.")
    exit()

# Reward scheme
#           Block   Reward  MN      Stake   Community fund
rewards = [[0,      495000, 0,      0,      0],
           [201,    5,      0,      0,      0],
           [14602,  175,    0.54,   0.36,   0.10],
           [52043,  95,     0.54,   0.36,   0.10],
           [139884, 25,     0.60,   0.25,   0.15],
           [665485, 20,     0.60,   0.20,   0.20],
           [1192527,15,     0.60,   0.15,   0.25]]

# Total ODIN supply in millions (assuming all claimed ODIN is withdrawn)
supply = locked_odin / 0.85
# Total number of masternodes
total_mn = int((locked_odin / 0.025)) + 20
# Blocks per day
bpd = (24 * 3600 / blocktime)
# Number of masternodes online
online_mn = int(total_mn * mn_online)
# Number of ODIN being staked (network weight)
online_staking = ((supply * 1000000) - (online_mn * 25000)) * staking_online

odin_start = odin
reward_period = 0
pending_stake = 0
pending_mn = 0
stake_total = 0
mn_total = 0

days = 30

for i in (range(current_block, current_block + int(bpd * days))):
    while (i > rewards[reward_period + 1][0]):
        reward_period += 1
        stake_reward =  rewards[reward_period][3] * rewards[reward_period][1]
        mn_reward =     rewards[reward_period][2] * rewards[reward_period][1]
    # Calculate stakes pending per block
    # (your weight / network weight) * stake percentage of block reward * block reward
    pending_stake += (staking / online_staking) * stake_reward
    if (pending_stake > stake_reward):
        odin += stake_reward
        stake_total += stake_reward
        pending_stake = 0
    pending_mn += (mn / online_mn) * mn_reward
    if (pending_mn > mn_reward):
        odin += mn_reward
        mn_total += mn_reward
        pending_mn = 0

perc_inc = (odin - odin_start) / odin_start * 100
perc_stake = stake_total / odin_start * 100
perc_mn = mn_total / odin_start * 100

print("In " + str(days) + " days, running " + str(mn) + " masternode(s) and staking " + str(staking) + " ODIN " +
      "increased your balance from " + str(round(odin_start, 2)) + " to " + str(round(odin, 2)) + ", a " + str(round(perc_inc, 2)) +  "% increase!")
print("Staking increased your balance by " + str(round(stake_total, 2)) + " ODIN (" + str(round(perc_stake, 2)) + "%)!")
print("Running " + str(mn) + " masternode(s) increased your balance by " + str(round(mn_total, 2)) + " ODIN (" + str(round(perc_mn, 2)) + "%)!")
